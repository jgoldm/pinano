#include <Tone.h>

Tone tone1;
Tone tone2;
Tone tone3;
Tone tone4;
Tone tone5;
Tone tone6;

// GLOBAL VARS
char spk1 = 53;

void setup() {
  // put your setup code here, to run once:
  tone1.begin(spk1);
  tone1.play(NOTE_C4);
  tone2.begin(spk1);
  tone2.play(NOTE_E4);
  tone3.begin(spk1);
  tone3.play(NOTE_G4);
  tone4.begin(spk1);
  tone4.play(NOTE_B5);
  tone5.begin(spk1);
  tone5.play(NOTE_D5);
  tone6.begin(spk1);
  tone6.play(NOTE_F5);
}

void loop() {
  // put your main code here, to run repeatedly:

}
