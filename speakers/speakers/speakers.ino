#include <Tone.h>

Tone tone1;
Tone tone2;
Tone tone3;
Tone tone4;
Tone tone5;
Tone tone6;

// GLOBAL VARS
char spk = 53;

void setup() {
  // put your setup code here, to run once:
  tone1.begin(spk);
  tone1.play(NOTE_C4);
  tone2.begin(spk);
  tone2.play(NOTE_E4);
  tone3.begin(spk);
  tone3.play(NOTE_G4);
  tone4.begin(spk);
  tone4.play(NOTE_F4);
}

void loop() {
  // put your main code here, to run repeatedly:

}
